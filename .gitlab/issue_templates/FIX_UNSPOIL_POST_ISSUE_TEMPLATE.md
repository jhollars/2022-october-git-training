## Description

The Good Dogs Project recently published a blog post about how to unspoil your dog. The content was reviewed and edited before publication, but one of their readers pointed out a few issues in the post. Your job is to fix the errors the reader found.

## Your tasks

You need to complete three tasks. You'll complete your work in the `/content/blog/how-to-unspoil-your-dog.md` file.

### Fix link syntax

- [ ] Fix the broken link in the **Ending co-sleeping** section.

### Change headings that use -ing 

- [ ] Change the headings that use -ing form to simple verb form.

### Fix a typo

- [ ] Fix the typo on line 32 in the **Ending feeding from the table** section.

## Before you open a merge request

- [ ] Review the issue to make sure you completed all tasks.
- [ ] Make sure that you committed your changes your branch, not `main`.
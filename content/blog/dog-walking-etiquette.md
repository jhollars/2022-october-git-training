---
title: Dog walking etiquette
date: 2022-09-18
draft: false
type: blog
lastmod: 2022-09-18
description: Dogs and their owners should strive to have good manners and be good citizens.
preview: /uploads/corgi-with-leash-smaller.jpg
---

According to the [Washington Post](https://www.washingtonpost.com/business/2022/01/07/covid-dogs-return-to-work/), 23 million Americans adopted dogs during the COVID-19 pandemic. That's nearly 1 in 5 households!

People have many good reasons for adopting dogs, including having a good friend around who will always be there for you. But one especially good reason is that your dog can act as a [commitment device](https://en.wikipedia.org/wiki/Commitment_device) to encourage you to exercise more. After all, [PetMD](https://www.petmd.com/news/view/how-often-should-you-walk-your-dog-37552) says your four-legged friend needs at least 10-15 minutes about two to three times a week.

Going on walks is good for both you and your dog. But it's also important to make sure you have good manners when you are out and about with your dog.


## Always use a leash

Using a leash is the single most important thing you can do. In many areas, it is more than just being polite: it's a legal requirement. Your dog's leash is there to protect you, your dog, and the people and property around you.

While many people find that having a dog gives them an easy way to talk to strangers, not everyone loves dogs. Studies show that 9% of people (about 1 out of 3) have a fear of dogs. To protect and show respect for people who may have a dog phobia, it's important to have a leash. Your dog's leash will allow you to keep your dog away from people (or other animals) who don't want your dog to smell them, rush them, or jump on them.

We recommend a 6-foot leash for most walks. Six feet is long enough to give your dog some freedom while also being short enough to quickly regain control of your dog if you need to.


## Bring along a plastic doggie bag

Don't be that dog owner: please pick up after your dog when they poop. To make this admittedly unpleasant task a little easier, buy some plastic doggie bags at your local pet store before going on your walk. (Yes, biodegradable bags are available!) Bring a few bags with you on every walk. Some bags can even come in a fancy dispenser that can attach to your waist!

When it's time to pick up after Fido, turn the bag inside out so that it can act as a glove to keep your hand clean. Try to pick it up as cleanly as possible. Use your free hand to fold the sides of the bag up and around the poop. Sometimes you may need more than one bag.

Tie up the bag securely, then bring the filled bag along with you on the rest of the walk and throw it away in your outside trash can when you return home. Make sure you follow any additional city rules and ordinances around cleaning up after your dog.


## Share the road and sidewalk

When you have to pass other walkers, dogs, or small vehicles on the road, keep your dog close to you and give other people enough space to safely and comfortably pass you and your dog. If you can see in advance that there won't be enough space, consider crossing to the other side of the road or temporarily stepping off the walkway to provide enough space. It will likely only take a moment of your time without causing inconvenience for other.


---

If you follow these simple guidelines, you'll demonstrate that you care about the people around you and your community. Having good manners means showing concern for other people's comfort and safety as much as you and your dogs. It's all part of being a good dog owner, citizen, and human being.
